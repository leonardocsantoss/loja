django<2.1
pillow
easy-thumbnails==2.5
django-ckeditor==5.4.0

# Deploy
pymysql
gunicorn
mysqlclient