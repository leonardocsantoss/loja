from __future__ import with_statement
from fabric.api import *


@with_settings(warn_only=True)
@hosts("webapps@loja.in4.com.br")
def deploy():
    with cd('/var/webapps/loja/loja/'):
        # get lastest version from git
        run('git pull')
        run('../bin/pip install -r requirements.txt')
        run('../bin/python manage.py migrate')
        run('../bin/python manage.py collectstatic --noinput')
        # restart supervisord
        run('supervisorctl restart loja')