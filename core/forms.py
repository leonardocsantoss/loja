from django import forms
from django.contrib.auth.models import User

from .models import Venda, Produto

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', )

    new_password = forms.CharField(label='Digite a senha', widget=forms.PasswordInput())
    new_password2 = forms.CharField(label='Digita a senha novamente', widget=forms.PasswordInput())

    def clean_new_password2(self):
        new_password = self.cleaned_data.get('new_password')
        new_password2 = self.cleaned_data.get('new_password2')
        if new_password != new_password2:
            raise forms.ValidationError(u'As duas senhas devem ser iguais.')
        return new_password2

    def save(self, commit=True):
        self.instance.set_password(self.cleaned_data.get('new_password'))
        return super(UserForm, self).save(commit=commit)

class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', )

    new_password = forms.CharField(label='Digite a senha', required=False, widget=forms.PasswordInput())
    new_password2 = forms.CharField(label='Digita a senha novamente', required=False, widget=forms.PasswordInput())

    def clean_new_password2(self):
        new_password = self.cleaned_data.get('new_password')
        new_password2 = self.cleaned_data.get('new_password2')
        if new_password or new_password2 and new_password != new_password2:
            raise forms.ValidationError(u'As duas senhas devem ser iguais.')
        return new_password2

    def save(self, commit=True):
        if self.cleaned_data.get('new_password'):
            self.instance.set_password(self.cleaned_data.get('new_password'))
        return super(UserUpdateForm, self).save(commit=commit)

class CompraForm(forms.ModelForm):
    class Meta:
        model = Venda
        fields = ('quantidade', )

    forma_pagamento = forms.ChoiceField(label='Forma de Pagamento', widget=forms.RadioSelect, choices=Venda.FORMA_PAGAMENTO_CHOICES)

    def save(self, compra_view, commit=True):
        venda = self.instance
        venda.forma_pagamento = self.cleaned_data.get('forma_pagamento')
        venda.status = 'a'
        produto = Produto.objects.get(slug=compra_view.kwargs.get('slug'))
        produto.quantidade -= 1
        venda.produto = produto
        venda.comprador = compra_view.request.user
        venda.valor = produto.valor * int(venda.quantidade)
        return super(CompraForm, self).save(commit=commit)