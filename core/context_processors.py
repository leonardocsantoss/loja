from core.models import Categoria

def core(request):
    return {
        'categorias': Categoria.objects.all()
    }
