# Generated by Django 2.0.3 on 2018-04-10 21:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20180403_1944'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='produto',
            options={'ordering': ('valor',), 'verbose_name': 'Produto', 'verbose_name_plural': 'Produtos'},
        ),
        migrations.AlterField(
            model_name='venda',
            name='data',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Data'),
        ),
    ]
