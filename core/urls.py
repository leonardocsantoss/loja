from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView

from .views import IndexView, CadastroView, CategoriaView, ProdutoView, CompraView, UserComprasView, UpdateUserView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('cadastro/', CadastroView.as_view(), name='cadastro'),
    path('update_user/', UpdateUserView.as_view(), name='update_user'),
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(next_page='index'), name='logout'),
    path('categoria/<slug:slug>/', CategoriaView.as_view(), name='categoria'),
    path('<slug:slug>/', ProdutoView.as_view(), name='produto'),
    path('<slug:slug>/comprar/', CompraView.as_view(), name='comprar'),
    path('<pk>/user/compras/', UserComprasView.as_view(), name='user_compras'),
]