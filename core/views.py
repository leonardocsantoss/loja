from django.views.generic import (
    TemplateView, DetailView, FormView, ListView,
    UpdateView
)
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User
from django.urls import reverse

from .models import Categoria, Produto, Venda
from .forms import UserForm, UserUpdateForm, CompraForm

class IndexView(ListView):
    template_name = "index.html"
    template_name_ajax = "ajax/produtos.html"
    queryset = Produto.objects.filter(status='P')
    paginate_by = 9

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        return [self.template_name]

class CategoriaView(ListView):
    template_name = 'categoria.html'
    template_name_ajax = "ajax/produtos.html"
    paginate_by = 9

    def get_template_names(self):
        if self.request.is_ajax():
            return [self.template_name_ajax]
        return [self.template_name]

    def get_object(self):
        return get_object_or_404(Categoria, slug=self.kwargs.get('slug'))

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return self.object.produto_set.filter(status='P')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = self.object
        return context

class ProdutoView(DetailView):
    model = Produto
    queryset = Produto.objects.filter(status='P')
    template_name = 'produto.html'

class UserComprasView(DetailView):
    model = User
    template_name = 'user_compras.html'

    def get_context_data(self, **kwargs):
        context = super(UserComprasView, self).get_context_data(**kwargs)
        context['compras'] = Venda.objects.filter(comprador=self.request.user)
        return context

class CadastroView(FormView):
    template_name = 'cadastro.html'
    form_class = UserForm

    def form_valid(self, form):
        user = form.save()
        auth_login(self.request, user)
        return redirect('index')

class CompraView(FormView):
    template_name = 'compra.html'
    form_class = CompraForm

    def get_context_data(self, **kwargs):
        context = super(CompraView, self).get_context_data(**kwargs)
        context['produto'] = Produto.objects.get(slug=self.kwargs.get('slug'))
        return context

    def form_valid(self, form):
        try:
            form.save(compra_view=self)
            return redirect(reverse('user_compras', kwargs={'pk': self.request.user.pk}))
        except Exception:
            return redirect('login')

class UpdateUserView(UpdateView):
    template_name = 'update_user.html'
    form_class = UserUpdateForm

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        user = form.save()
        auth_login(self.request, user)
        return redirect('index')