from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_save, post_delete
from django.utils.text import slugify
from django.dispatch import receiver

from django.utils.safestring import mark_safe
from django.urls import reverse

from ckeditor_uploader.fields import RichTextUploadingField

class Categoria(models.Model):
    class Meta:
        ordering = ('nome', )

    nome = models.CharField(u'Nome', max_length=120, help_text=u'Informe o nome do produto.')
    slug = models.SlugField(u'URL', unique=True, max_length=120, blank=True)

    def get_absolute_url(self):
        return reverse('categoria', kwargs={'slug': self.slug})

    def __str__(self):
        return self.nome

@receiver(pre_save, sender=Categoria)
def categoria_slug_pre_save(sender, instance, raw, using, update_fields, **kwargs):
    if not instance.slug:
        new_slug = slugify(instance.nome)
        instance.slug = new_slug
        num = 1
        while Categoria.objects.filter(slug=instance.slug).exists():
            instance.slug = u'%s-%s' % (new_slug, num)
            num += 1

class Produto(models.Model):
    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'
        ordering = ('valor', )

    STATUS_CHOICES = (
        ('P', u'Publicado'),
        ('R', u'Rascunho'),
    )

    nome = models.CharField(u'Nome', max_length=120, help_text=u'Informe o nome do produto.')
    slug = models.SlugField(u'URL', unique=True, max_length=120, blank=True)
    status = models.CharField(u'Status', max_length=1, choices=STATUS_CHOICES)
    data_criacao = models.DateTimeField(u'Criado em', auto_now_add=True)
    data_atualizacao = models.DateTimeField(u'Ultima Atualização', auto_now=True)
    valor = models.DecimalField(u'Valor', max_digits=10, decimal_places=2)
    descricao = RichTextUploadingField(u'Descrição', blank=True)
    imagem = models.ImageField(u'Imagem', upload_to='produtos')
    categoria = models.ForeignKey(Categoria, on_delete=models.PROTECT)
    autor = models.ForeignKey(User, blank=True, on_delete=models.PROTECT)
    quantidade = models.PositiveIntegerField(u'Quantidade', default=0)

    def get_absolute_url(self):
        return reverse('produto', kwargs={'slug': self.slug})

    def __str__(self):
        return self.nome

@receiver(pre_save, sender=Produto)
def produto_slug_pre_save(sender, instance, raw, using, update_fields, **kwargs):
    if not instance.slug:
        new_slug = slugify(instance.nome)
        instance.slug = new_slug
        num = 1
        while Produto.objects.filter(slug=instance.slug).exists():
            instance.slug = u'%s-%s' % (new_slug, num)
            num += 1

@receiver(post_delete, sender=Produto)
def produto_imagem_post_delete(sender, instance, *args, **kwargs):
    try: instance.imagem.delete()
    except: pass

class Venda(models.Model):
    class Meta:
        ordering = ('data', )
        verbose_name = 'Venda'
        verbose_name_plural = 'Vendas'

    STATUS_CHOICES = (
        ('a', 'Aprovado'),
        ('e', 'Esperando'),
        ('r', 'Reprovado'),
    )

    FORMA_PAGAMENTO_CHOICES = (
        ('b', 'Boleto'),
        ('c', 'Cartão'),
    )

    produto = models.ForeignKey(Produto, on_delete=models.PROTECT)
    comprador = models.ForeignKey(User, on_delete=models.PROTECT)
    data = models.DateTimeField(u'Data', auto_now_add=True)
    valor = models.DecimalField(u'Valor', max_digits=10, decimal_places=2)
    quantidade = models.PositiveIntegerField(u'Quantidade')
    forma_pagamento = models.CharField(u'Forma de Pagamento', max_length=1, choices=FORMA_PAGAMENTO_CHOICES)
    status = models.CharField(u'Status', max_length=1, choices=STATUS_CHOICES)

    def __str__(self):
        return "%s comprou %s" %(self.comprador, self.produto)

class ProdutoImagem(models.Model):

    class Meta:
        verbose_name = 'Imagem'
        verbose_name_plural = 'Imagens'

    produto = models.ForeignKey(Produto, on_delete=models.CASCADE)
    imagem = models.ImageField(u'Imagem', upload_to='produtos')
    ordem = models.PositiveIntegerField(u'Ordem')

    def __str__(self):
        return "%s" % self.produto

@receiver(post_delete, sender=ProdutoImagem)
def produtoimagem_imagem_post_delete(sender, instance, *args, **kwargs):
    try: instance.imagem.delete()
    except: pass