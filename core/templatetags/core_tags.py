from django import template

register = template.Library()


@register.filter
def total(cl):
    total = 0.0
    for venda in cl.result_list:
        total += float(venda.valor * venda.quantidade)
    return total