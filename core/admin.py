from django.contrib import admin, messages

from core.models import Produto, Categoria, Venda, ProdutoImagem

@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('nome', 'slug', )
    search_fields = ('nome', )
    fieldsets = (
        (None, {
            'fields': ('nome', )
        }),
    )

class ProdutoImagemInline(admin.TabularInline):
    model = ProdutoImagem
    extra = 1
    verbose_name = u'Imagem'
    verbose_name_plural = u'Imagens'

@admin.register(Produto)
class ProdutoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'valor', 'status', 'autor', 'categoria', )
    search_fields = ('nome', )
    list_filter = ('status', 'autor', 'categoria', 'valor', )
    date_hierarchy = 'data_criacao'
    radio_fields = {'status': admin.HORIZONTAL}
    autocomplete_fields = ('autor', 'categoria', )
    inlines = (ProdutoImagemInline, )
    list_editable = ('status', )
    list_per_page = 10

    fieldsets = (
        (None, {
            'fields': ('nome', 'valor', 'quantidade', )
        }),
        ('Imagem', {
            'fields': ('imagem', ),
        }),
        ('Outros', {
            'fields': ('descricao', 'categoria', 'status', ('data_criacao', 'data_atualizacao', ), ),
        }),
    )
    readonly_fields = ('data_criacao', 'data_atualizacao', )

    def save_model(self, request, obj, form, change):
        if not change:
            obj.autor = request.user
        return super().save_model(request, obj, form, change)

@admin.register(Venda)
class VendaAdmin(admin.ModelAdmin):
    list_display = ('produto', 'comprador', 'data', 'total', 'forma_pagamento', 'status', )
    list_filter = ('produto', 'comprador', 'data', 'forma_pagamento', 'status', )
    date_hierarchy = 'data'
    radio_fields = {'status': admin.HORIZONTAL, 'forma_pagamento': admin.HORIZONTAL}
    readonly_fields = ('data', )
    actions = ('soma', )

    fieldsets = (
        ('Dados', {
            'fields': ('produto', 'comprador', 'data', )
        }),
        ('Valor', {
            'fields': ('valor', 'quantidade', 'forma_pagamento', ),
        }),
        ('Situação', {
            'fields': ('status', ),
        }),
    )

    def soma(self, request, queryset):
        total = 0.0
        for venda in queryset:
            total += float(venda.valor * venda.quantidade)
        messages.info(request, u'Total vendido: R$ %.2f' % total)
    soma.short_description = u'Realizar soma'

    def total(self, obj):
        return 'R$ %.2f' % float(obj.valor * obj.quantidade)