$(window).scroll(function() {
  if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {

    $('.ajax-pagination').each(function(index, el) {
      var page = $(el).attr('data-page')*1;
      var max_page = $(el).attr('data-max_page')*1;
      var block = $(el).attr('data-block')*1;

      if(page < max_page && block == 0){
        page += 1;
        $(el).attr('data-block', 1);
        $.ajax({
          method: "GET",
          url: "",
          data: {page: page}
        }).done(function(html) {
          $(el).append(html);
          $(el).attr('data-page', page);
          $(el).attr('data-block', 0);
        });
      }
    });
  }
});
