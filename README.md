# Como iniciar o projeto (dev)

1. Criar virtualenv
> virtualenv loja -ppython3

2. Ativar virtualenv
> cd loja
> . bin/activate

3. Clonar o repositório
> git clone git@bitbucket.org:leonardocsantoss/loja.git

4. Entrar no repositório
> cd loja

5. Instalar as libs
> pip install -r pip instarequirements.txt

6. Rodar o migrate
> python manage.py migrate

7. Cria o superuser
> python manage.py createsuperuser

8. Rodar o servidor de testes
> python manage.py runserver